package com.Odroid.cisne.web;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class TemplatesController {

	private ObjectMapper jacksonMapper = new ObjectMapper();

	@RequestMapping(value = "templates.html")
	public String templates(Model model,
			@RequestParam(required = false) String key,
			@RequestParam(required = false) String param) throws IOException {
		model.addAttribute("tab", "notes");
		if (key == null) {
			model.addAttribute("key", "");
			model.addAttribute("text", "No text to templatify");
		} else {
			model.addAttribute("key", key);
			TemplatifyRequest templatifyRequest = jacksonMapper.readValue(
					param, TemplatifyRequest.class);
			model.addAttribute("text", templatifyRequest.getContent());
		}
		return "WEB-INF/views/templates/templates.jsp";
	}

	public static class TemplatifyRequest {
		private String content;

		public TemplatifyRequest() {
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

	}
}
