package com.Odroid.cisne.fw.core;

import java.util.Locale;
import java.util.Properties;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

public class FwReloadableResourceBundleMessageSource extends
		ReloadableResourceBundleMessageSource {
	public Properties getAllProperties(Locale locale) {
		Properties p = super.getMergedProperties(locale).getProperties();
		return p;
	}

}
