package com.Odroid.cisne.fw.core;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

public class FwUserDetailsService extends JdbcDaoImpl {
	private String idByUsernameQuery;
	private final static String DEF_ID_BY_USERNAME_QUERY = "select id from users where username = ?";

	public FwUserDetailsService() {
		super();
		setIdByUsernameQuery(DEF_ID_BY_USERNAME_QUERY);
	}

	@Override
	protected FwUser createUserDetails(String username,
			UserDetails userFromUserQuery,
			List<GrantedAuthority> combinedAuthorities) {
		String returnUsername = userFromUserQuery.getUsername();
		Long id = getJdbcTemplate().queryForObject(idByUsernameQuery,
				new String[] { username }, Long.class);
		return new FwUser(returnUsername, userFromUserQuery.getPassword(),
				userFromUserQuery.isEnabled(), true, true, true,
				combinedAuthorities, id);
	}

	public String getIdByUsernameQuery() {
		return idByUsernameQuery;
	}

	public void setIdByUsernameQuery(String idByUsernameQuery) {
		this.idByUsernameQuery = idByUsernameQuery;
	}
}
