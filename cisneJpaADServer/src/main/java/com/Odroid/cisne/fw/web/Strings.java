package com.Odroid.cisne.fw.web;

import java.util.Locale;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Odroid.cisne.fw.core.FwReloadableResourceBundleMessageSource;

@Controller
public class Strings {

	@Autowired
	@Qualifier("messageSource")
	FwReloadableResourceBundleMessageSource messageSource;

	@RequestMapping(value = "/public/strings.json", method = RequestMethod.GET)
	public @ResponseBody
	Properties delete(Locale locale) {
		Properties p = messageSource.getAllProperties(locale);
		return p;
	}

}
