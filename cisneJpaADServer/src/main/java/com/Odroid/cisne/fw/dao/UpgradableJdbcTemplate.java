package com.Odroid.cisne.fw.dao;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

public class UpgradableJdbcTemplate extends NamedParameterJdbcTemplate {
	private final Log logger = LogFactory.getLog(getClass());
	private final static String SQL_GETVERSION = "SELECT version FROM meta_version";
	private final static String SQL_SETVERSION = "INSERT INTO meta_version (version) values (:version)";

	public UpgradableJdbcTemplate(DataSource dataSource,
			List<Resource> scripts, Resource versionResource) {
		super(dataSource);
		upgradeIfNeeded(dataSource, scripts, versionResource);
	}

	private void upgradeIfNeeded(DataSource dataSource, List<Resource> scripts,
			Resource versionResource) {
		boolean mustUpgrade = true;
		long wantedVersion = 0;
		try {
			String wantedVersionString = IOUtils.toString(versionResource
					.getInputStream());
			wantedVersion = Long.parseLong(wantedVersionString);
			long currentVersion = getJdbcOperations().queryForObject(
					SQL_GETVERSION, Long.class);
			if (currentVersion == wantedVersion) {
				mustUpgrade = false;
			}
			logger.info("Requested version " + wantedVersion
					+ "; current version " + currentVersion);
		} catch (Exception e) {
			logger.info("Error retrieving version from existing DB. Creating schema.");
		}
		if (!mustUpgrade) {
			return;
		}
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		populator.setIgnoreFailedDrops(false);
		for (Resource script : scripts) {
			populator.addScript(script);
		}
		org.springframework.jdbc.datasource.init.DatabasePopulatorUtils
				.execute(populator, dataSource);
		SqlParameterSource params = new MapSqlParameterSource("version",
				wantedVersion);
		update(SQL_SETVERSION, params);
	}
}
