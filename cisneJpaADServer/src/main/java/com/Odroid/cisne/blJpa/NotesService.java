package com.Odroid.cisne.blJpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.domain.Note;

@Service("notesService")
@Transactional(readOnly = true)
public class NotesService implements INotesService {

	@PersistenceContext
	private EntityManager em;

	@Override
	@PreAuthorize("#userId == principal.id")
	public List<Note> search(String text, @P("userId") Long userId) {
		return em
				.createQuery(
						"FROM Note n "
								+ "WHERE ((LOWER(n.title) LIKE LOWER(:text)) OR (LOWER(n.content) LIKE LOWER(:text))) AND n.userId = :userId "
								+ "ORDER BY n.title", Note.class)
				.setParameter("text", "%" + text + "%")
				.setParameter("userId", userId).getResultList();
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("#note.userId == principal.id")
	public Long add(@P("note") Note note) {
		note.setId(null);
		em.persist(note);
		return note.getId();
	}

	@Override
	@PostAuthorize("principal.id == returnObject.userId")
	public Note get(Long id) {
		return em.find(Note.class, id);
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("@notesService.get(#note.id).userId == principal.id")
	public void save(@P("note") Note note) {
		Note currentNote = get(note.getId());
		currentNote.setTitle(note.getTitle());
		currentNote.setContent(note.getContent());
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("@notesService.get(#id).userId == principal.id")
	public void delete(@P("id") Long id) {
		em.remove(get(id));
	}

	@Override
	public Long searchCount(String text, Long userId) {
		return em
				.createQuery(
						"SELECT COUNT (*) FROM Note n "
								+ "WHERE ((LOWER(n.title) LIKE LOWER(:text)) OR (LOWER(n.content) LIKE LOWER(:text))) AND n.userId = :userId ",
						Long.class).setParameter("text", "%" + text + "%")
				.setParameter("userId", userId).getSingleResult();
	}

	@Override
	public Note searchGet(String text, Long index, Long userId) {
		return em
				.createQuery(
						"FROM Note n "
								+ "WHERE ((LOWER(n.title) LIKE LOWER(:text)) OR (LOWER(n.content) LIKE LOWER(:text))) AND n.userId = :userId "
								+ "ORDER BY n.title", Note.class)
				.setParameter("text", "%" + text + "%")
				.setParameter("userId", userId)
				.setFirstResult(index.intValue()).setMaxResults(1)
				.getSingleResult();
	}

}
