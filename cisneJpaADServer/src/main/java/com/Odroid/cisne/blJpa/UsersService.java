package com.Odroid.cisne.blJpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.Authority;
import com.Odroid.cisne.domain.User;
import com.Odroid.cisne.fw.core.AuditableAnnotation;

@Service("usersService")
@Transactional(readOnly = true)
public class UsersService implements IUsersService {
	private final Log logger = LogFactory.getLog(getClass());

	@PersistenceContext
	private EntityManager em;

	@Autowired
	@Qualifier("bcryptEncoder")
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public List<User> getUsers() {
		return em.createQuery("FROM User ORDER BY username", User.class)
				.getResultList();
	}

	@Override
	public User getUser(Long id) {
		return em.find(User.class, id);
	}

	@Override
	public User getUser(String username) {
		return em
				.createQuery("FROM User WHERE username = :username", User.class)
				.setParameter("username", username).getSingleResult();
	}

	@Override
	public boolean isAdmin(String username) {
		// TODO: tiene sentido este m�todo? Siempre que llama aqu� ya ha hecho
		// getUser(username) antes...
		Long count = em
				.createQuery(
						"SELECT COUNT (*) FROM Authority a "
								+ "WHERE a.user.username = :username AND a.authority = :authority",
						Long.class).setParameter("username", username)
				.setParameter("authority", Authority.ROLE_ADMIN)
				.getSingleResult();
		if (count == 0) {
			return false;
		} else if (count == 1) {
			return true;
		} else {
			throw new RuntimeException("Should not have reached here. " + count
					+ " ROLE_ADMIN assigned to " + username);
		}
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@AuditableAnnotation
	public Long addUser(User user, boolean admin) {
		// TODO: no tiene sentido que reciba un User pero ignore algunos de sus
		// atributos. Una clase para el domain y otra para el API de servicio?
		if (user.getPassword().isEmpty()) {
			throw new EmptyPasswordException();
		}
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		Authority userAuthority = new Authority(null, Authority.ROLE_USER, user);
		user.getAuthorities().add(userAuthority);
		if (admin) {
			Authority adminAuthority = new Authority(null,
					Authority.ROLE_ADMIN, user);
			user.getAuthorities().add(adminAuthority);
		}
		em.persist(user);
		return user.getId();
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@AuditableAnnotation
	public void saveUser(User user, boolean admin) {
		// TODO: no tiene sentido que reciba un User pero ignore algunos de sus
		// atributos. Una clase para el domain y otra para el API de servicio?
		// TODO: Confuso: add(u); save(u); login(u); falla porque save(u)
		// modifica la passwd si no es vacio
		User currentUser = getUser(user.getId());
		if (!user.getPassword().isEmpty()) {
			logger.info("Changing password for user " + user.getUsername());
			String encodedPassword = passwordEncoder.encode(user.getPassword());
			currentUser.setPassword(encodedPassword);
		}
		// Bug: si cambio el username, peta, porque es FK. Solucion: primero
		// quito todas las authorities, luego cambio el username, y luego las
		// anado otra vez... pero paso de hacerlo en un ejemplo
		if (!currentUser.getUsername().equals(user.getUsername())) {
			throw new UnsupportedOperationException("Not yet implemented");
		}
		currentUser.setName(user.getName());
		currentUser.setEnabled(user.getEnabled());
		List<Authority> authorities = currentUser.getAuthorities();
		Authority userAuthority = null;
		Authority adminAuthority = null;
		for (Authority authority : authorities) {
			if (authority.getAuthority().equals(Authority.ROLE_USER)) {
				userAuthority = authority;
			} else if (authority.getAuthority().equals(Authority.ROLE_ADMIN)) {
				adminAuthority = authority;
			} else {
				logger.warn("Unexpected authority " + authority.getAuthority()
						+ " for user " + currentUser.getUsername());
			}
		}
		if (userAuthority == null) {
			userAuthority = new Authority(null, Authority.ROLE_USER,
					currentUser);
			currentUser.getAuthorities().add(userAuthority);
		}
		if (admin) {
			if (adminAuthority == null) {
				adminAuthority = new Authority(null, Authority.ROLE_ADMIN,
						currentUser);
				currentUser.getAuthorities().add(adminAuthority);
			}
		} else {
			if (adminAuthority != null) {
				currentUser.getAuthorities().remove(adminAuthority);
			}
		}
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@AuditableAnnotation
	public void deleteUser(long id) {
		User u = getUser(id);
		em.remove(u);
		// NOTE: performance problem. Get: 1 select del usuario. Borrado en
		// cascada: 1 select para las authorities, +1 por cada authority para el
		// usuario (LAZY ignorado), +1 por cada delete de authority. Borrado: 1
		// query
		// 7 queries donde deber�an ser 2
	}

}
