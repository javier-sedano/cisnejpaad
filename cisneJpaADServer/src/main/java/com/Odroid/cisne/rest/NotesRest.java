package com.Odroid.cisne.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.domain.Note;
import com.Odroid.cisne.fw.core.FwUser;

@RestController
@RequestMapping("rest/notes")
public class NotesRest {
	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	@Qualifier("notesService")
	private INotesService notesService;

	@RequestMapping(value = "searchCount", method = RequestMethod.GET)
	public Long count(@RequestParam String filter, Authentication principal) {
		Long userId = ((FwUser) principal.getPrincipal()).getId();
		Long count = notesService.searchCount(filter, userId);
		return count;
	}

	@RequestMapping(value = "searchGet", method = RequestMethod.GET)
	public Note get(@RequestParam String filter, @RequestParam Long index,
			Authentication principal) {
		Long userId = ((FwUser) principal.getPrincipal()).getId();
		Note note = notesService.searchGet(filter, index, userId);
		note.setContent(""); // Not needed in this API method
		return note;
	}

	@RequestMapping(value = "addEmpty", method = RequestMethod.GET)
	public Long addEmpty(Authentication principal) {
		Long userId = ((FwUser) principal.getPrincipal()).getId();
		Note note = new Note(null, "Note" + System.currentTimeMillis(), "",
				userId);
		Long id = notesService.add(note);
		return id;
		// If it fails it will return an HTTP error
	}

	@RequestMapping(value = "get", method = RequestMethod.GET)
	public Note get(Model model, @RequestParam Long id) {
		Note note = notesService.get(id);
		return note;
	}

	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public Long delete(@RequestParam Long id) {
		notesService.delete(id);
		return 0L;
		// If it fails it will return an HTTP error
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public Long save(Model model, @RequestBody NoteApi noteApi,
			Authentication principal) {
		logger.info("Save " + noteApi);
		Long userId = ((FwUser) principal.getPrincipal()).getId();
		Note note = new Note(noteApi.getId(), noteApi.getTitle(),
				noteApi.getContent(), userId);
		notesService.save(note);
		logger.info("Save returning");
		return note.getId();
		// If it fails it will return an HTTP error
	}

	public static class NoteApi {
		protected Long id = -1L;
		protected String title = "";
		private String content = "c";

		public NoteApi() {
		}

		public NoteApi(Long id, String title, String content) {
			super();
			this.id = id;
			this.title = title;
			this.content = content;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		@Override
		public String toString() {
			return "NoteApi [id=" + id + ", title=" + title + ", content="
					+ content + "]";
		}

	}

}
