package com.Odroid.cisne.bl;

import java.util.List;

import com.Odroid.cisne.domain.User;

public interface IUsersService {

	public List<User> getUsers();

	public User getUser(Long id);

	public User getUser(String username);

	public boolean isAdmin(String username);

	public Long addUser(User user, boolean admin);

	public void saveUser(User user, boolean admin);

	public void deleteUser(long id);

	public static class EmptyPasswordException extends IllegalArgumentException {
		private static final long serialVersionUID = 1L;

		public EmptyPasswordException() {
			super();
		}

		public EmptyPasswordException(String message, Throwable cause) {
			super(message, cause);
		}

		public EmptyPasswordException(String s) {
			super(s);
		}

		public EmptyPasswordException(Throwable cause) {
			super(cause);
		}
	}

}
