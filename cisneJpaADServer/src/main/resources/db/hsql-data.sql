INSERT INTO users(id, username, password, enabled, name) VALUES (2, 'q', '$2a$10$sLWOIeHVsAawsfHU3buRpOzSRpHzQxIUqX4Unb8R1wn/GGH3nHCJm', 1, 'Bonifacio Buendia');
INSERT INTO authorities(username, authority) VALUES ('q', 'ROLE_USER');
INSERT INTO users(id, username, password, enabled, name) VALUES (3, 'c', '$2a$10$hV3U6r9wWv2DC./WvTkcre/ipfFGPNDevGKx6Db6TyaLlhvQjPMUe', 0, 'Carlos Casado Castro');
INSERT INTO authorities(username, authority) VALUES ('c', 'ROLE_USER');

INSERT INTO notes(title, content, userId) VALUES ('one', 'One ring to the darlk lord''s hand', 1);
INSERT INTO notes(title, content, userId) VALUES ('lorem', 'Lorem ipsum dolor sit amet', 1);
INSERT INTO notes(title, content, userId) VALUES ('digo', 'Donde voy a llegar', 1);
INSERT INTO notes(title, content, userId) VALUES ('nothing', 'So close, no matter how far', 1);
INSERT INTO notes(title, content, userId) VALUES ('two', 'One, two, three o''clock, four o''clock rock', 2);
INSERT INTO notes(title, content, userId) VALUES ('hal', 'Dave', 3);
