<%@ include file="bits/html-head.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">
	fw.nav.cleanKeys();
</script>

<div id="main">
	<div id="header">
		<div id="logo"></div>
	</div>

	<div id="site_content">

		<form action="<c:url value="/login.html"/>" method="POST">

			<c:if test="${!empty signonForwardAction}">
				<input type="hidden" name="forwardAction"
					value="<c:url value="${signonForwardAction}"/>" />
			</c:if>
			<div class="center">

				<p>
					<spring:message code="common.login.loginLabel" />
					<input type="text" name="username" value="" />
				</p>
				<p>
					<spring:message code="common.login.passwordLabel" />
					<input type="password" name="password" value="" />
				</p>
				<p>
					<input type="submit"
						value="<spring:message code="common.login.loginButton" />"
						name="update" />
				</p>
				<p>
					Or download a thick <a href="<c:url value="/public/swCisne.jar" />">Java
						Swing client</a> preconfigured to connect to this server.
				</p>
			</div>
		</form>

	</div>
	<div id="footer">
		<p>
			<spring:message code="common.login.copyright" />
		</p>
	</div>

</div>

<%@ include file="bits/html-foot.jsp"%>
