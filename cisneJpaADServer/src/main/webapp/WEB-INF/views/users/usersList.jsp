<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ul>
	<c:forEach items="${usersList}" var="user">
		<li><a href="#" onclick="users.show(${user.id}); return false;">${user.username}</a></li>
	</c:forEach>
</ul>
