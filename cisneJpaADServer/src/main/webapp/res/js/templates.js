var templates = {};
$(document).ready(function() {
	fw.nav.registerNoReply($('#key').val());
});

templates.templatifySentences = function() {
	var text = $('#text').text();
	var res = text.replace(/\./g, '.\n');
	$('#after').text(res);
};

templates.templatifyWords = function() {
	var text = $('#text').text();
	var res = text.replace(/\ /g, '\n');
	$('#after').text(res);
};

templates.templatifyAccept = function() {
	var key = $('#key').val();
	var reply = $('#after').text();
	fw.nav.reply(key, reply);
};
