// Add startsWith to String
if (typeof String.prototype.startsWith != 'function') {
	// see below for better implementation!
	String.prototype.startsWith = function(str) {
		return this.indexOf(str) == 0;
	};
}

var fw = {};
fw.debug = {};
fw.ajax = {};
fw.i18n = {};
fw.ui = {};
fw.nav = {};

fw.debug.var_dump = function(o) {
	var text = "";
	for ( var n in o) {
		text += n + ": " + o[n] + "\n";
	}
	alert(text);
};

fw.debug.var_console = function(o) {
	var text = "";
	for ( var n in o) {
		text += n + ": " + o[n] + "\n";
	}
	fw.debug.console(text);
};

fw.debug.console = function(text) {
	if (window.console) {
		console.log(text);
	}
};

fw.ajax.error = function(prefix) {
	return function(data) {
		var text = prefix + "\n" + data.responseText;
		fw.debug.console(text);
		fw.ui.message(fw.i18n.s["common.dialog.error"], prefix,
				data.responseText);
	};
};

fw.ajax.loadComplete = function(error_prefix, successCallback) {
	return function(response, status, xhr) {
		if (status == "error") {
			var text = error_prefix + "\n" + response;
			fw.debug.console(text);
			fw.ui.message(fw.i18n.s["common.dialog.error"], error_prefix,
					response);
		} else {
			successCallback(response, status, xhr);
		}
	};
};

fw.i18n.s = {};

fw.i18n.load = function() {
	var url = url_public + "strings.json";
	var onGetSuccess = function(data) {
		fw.i18n.s = data;
	};
	var onGetError = fw.ajax
			.error("Unable to load internationalization strings");
	$.get(url).success(onGetSuccess).error(onGetError);

};

fw.ui.message = function(title, text, details) {
	$('body').append(
			'<div id="fw_ui_message" title="' + title + '">' + text + '</div>');
	var onOk = function() {
		$(this).dialog("close");
		$('#fw_ui_message').remove();
	};
	var buttons = {
		Ok : onOk
	};
	var width = 300;
	var height = 200;
	if (details !== undefined) {
		buttons.Details = function() {
			$(this).dialog("close");
			$('#fw_ui_message').remove();
			fw.ui.message(title, details);
		};
	}
	if (text.length > 200) {
		width = 600;
		height = 400;
	}
	$("#fw_ui_message").dialog({
		modal : true,
		buttons : buttons,
		width : width,
		height : height,
		close : onOk
	});
};

fw.nav.callbacks = {};

fw.nav.onStorageEvent = function(storageEvent) {
	if (storageEvent.storageArea === localStorage) {
		if (fw.nav.callbacks[storageEvent.key] !== undefined) {
			var callback = fw.nav.callbacks[storageEvent.key];
			delete fw.nav.callbacks[storageEvent.key];
			var result = ((storageEvent.newValue === null) ? null : JSON
					.parse(storageEvent.newValue));
			localStorage.removeItem(storageEvent.key);
			$("#fw_modal").dialog("close");
			$("#fw_modal").remove();
			callback(result);
		}
	}
};

window.addEventListener('storage', fw.nav.onStorageEvent, false);

fw.nav.ask = function(url, param, modal, callback) {
	if (modal) {
		$('body').append(
				'<div id="fw_modal" title="">' + fw.i18n.s["common.waiting"]
						+ '</div>');
		var width = 300;
		var height = 200;
		$("#fw_modal").dialog({
			modal : true,
			width : width,
			height : height,
			closeOnEscape : false,
			open : function(event, ui) {
				$(".ui-dialog-titlebar-close").hide();
			}
		});
	}
	$('body')
			.append(
					'<form id="fw_navigationForm" action="'
							+ url
							+ '" method="post" target="_blank">'
							+ '<input id="fw_navigationForm_key" name="key" type="hidden" value=""/>'
							+ '<input id="fw_navigationForm_param" name="param" type="hidden" value=""/>'
							+ '</form>');
	var paramJson = JSON.stringify(param);
	var key = fw.nav.randomKey();
	$('#fw_navigationForm_key').val(key);
	$('#fw_navigationForm_param').val(paramJson);
	fw.nav.callbacks[key] = callback;
	$('#fw_navigationForm').submit();
	$('#fw_navigationForm').remove();
};

fw.nav.reply = function(key, response) {
	if (localStorage.getItem(key) === "") {
		var responseJson = JSON.stringify(response);
		localStorage.setItem(key, responseJson);
		window.close();
	} else {
		throw 'Error: localStorage["' + key + '"] is not and empty string ('
				+ localStorage.getItem(key) + ')';
	}
};

fw.nav.registerNoReply = function(key) {
	// (https://bugs.eclipse.org/bugs/show_bug.cgi?id=351470)
	// This hack removes the warning :-(
	var lastOnbeforeunload = window.onbeforeunload;
	var kk = lastOnbeforeunload;
	delete kk;
	window.onbeforeunload = function(e) {
		if (localStorage.getItem(key) === "") {
			// That is, not set by fw.nav.reply
			localStorage.setItem(key, null);
			if ((lastOnbeforeunload !== undefined)
					&& (lastOnbeforeunload !== null)) {
				lastOnbeforeunload(e);
			}
		}
	};
};

fw.nav.randomKey = function() {
	var r = Math.random();
	var rs = "fw.nav.ask_reply_" + r;
	if (localStorage.getItem(rs) === null) {
		localStorage.setItem(rs, "");
		return rs;
	} else {
		return fw.nav.randomKey();
	}
};

fw.nav.cleanKeys = function() {
	var l = localStorage.length;
	for (var i = l - 1; i >= 0; i--) {
		console.debug("Cleaning " + i);
		var k = localStorage.key(i);
		console.debug("  " + k);
		if (k.startsWith("fw.nav.ask_reply_")) {
			localStorage.removeItem(k);
		}
	}
};
