package com.Odroid.cisne.blJpa;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.Note;
import com.Odroid.cisne.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"/com/Odroid/cisne/blJpa/spring-bl-context.xml",
		"/com/Odroid/cisne/blJpa/spring-context.xml" })
// @TestExecutionListeners(listeners = {
// WithSecurityContextTestExcecutionListener.class })
@Transactional
public class NotesServiceTest {

	@Autowired
	@Qualifier("notesService")
	private INotesService notesService;

	@Autowired
	@Qualifier("usersService")
	private IUsersService usersService;

	@Test
	public void crudTest() {
		String title = "hey oh";
		String content = "let's go";
		Note n = new Note(-1L, title, content, userId);
		Long id = notesService.add(n);
		Note n2 = notesService.get(id);
		Assert.assertEquals(title, n2.getTitle());
		Assert.assertEquals(content, n2.getContent());
		Assert.assertEquals(userId, n2.getUserId());
		n2.setTitle(title + "2");
		n2.setContent(content + "2");
		notesService.save(n2);
		Note n3 = notesService.get(id);
		Assert.assertEquals(n2.getTitle(), n3.getTitle());
		Assert.assertEquals(n2.getContent(), n3.getContent());
		Assert.assertEquals(n2.getUserId(), n3.getUserId());
		List<Note> notes = notesService.search(title, userId);
		if (notes.size() < 1) {
			Assert.fail();
		}
		notesService.delete(id);
	}

	private Long userId = 0L;

	@Before
	public void addUser() {
		String username = "jsnow";
		String password = "qwerty";
		String name = "Jon Snow";
		int enabled = 1;
		User u = new User(-1L, username, password, enabled, name);
		Long id = usersService.addUser(u, true);
		userId = id;
	}

}
