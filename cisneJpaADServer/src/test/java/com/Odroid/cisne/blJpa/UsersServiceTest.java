package com.Odroid.cisne.blJpa;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"/com/Odroid/cisne/blJpa/spring-bl-context.xml",
		"/com/Odroid/cisne/blJpa/spring-context.xml" })
@Transactional
public class UsersServiceTest {

	@Autowired
	@Qualifier("usersService")
	private IUsersService usersService;

	@Autowired
	@Qualifier("bcryptEncoder")
	private BCryptPasswordEncoder bcryptEncoder;

	@Test
	public void crudTest() {
		String username = "jsnow";
		String password = "qwerty";
		String name = "Jon Snow";
		Integer enabled = 1;
		User u = new User(-1L, username, password, enabled, name);
		Long id = usersService.addUser(u, true);
		Assert.assertTrue(usersService.isAdmin(username));
		// Save without changing password
		User u2 = new User(id, username, "", enabled, name);
		usersService.saveUser(u2, false);
		Assert.assertFalse(usersService.isAdmin(username));
		User u3 = usersService.getUser(id);
		Assert.assertEquals(username, u3.getUsername());
		Assert.assertEquals(name, u3.getName());
		Assert.assertEquals(enabled, u3.getEnabled());
		Assert.assertTrue(password + ": " + u3.getPassword(),
				bcryptEncoder.matches(password, u3.getPassword()));
		usersService.deleteUser(id);
	}

}
