package com.Odroid.cisne.domain;

public class Note extends NoteMeta {
	private String content = "c";

	public Note() {
	}

	public Note(Long id, String title, String content) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Note [id=" + id + ", title=" + title + ", content=" + content
				+ "]";
	}

}
