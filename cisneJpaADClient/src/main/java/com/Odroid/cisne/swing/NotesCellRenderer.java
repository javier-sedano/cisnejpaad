package com.Odroid.cisne.swing;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import com.Odroid.cisne.swing.NotesListModel.NoteModel;

@org.springframework.stereotype.Component("notesCellRenderer")
public class NotesCellRenderer extends DefaultListCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		Component c = super.getListCellRendererComponent(list, value, index,
				isSelected, cellHasFocus);
		if (c instanceof DefaultListCellRenderer) {
			DefaultListCellRenderer defaultListCellRenderer = (DefaultListCellRenderer) c;
			NoteModel noteModel = (NoteModel) value;
			defaultListCellRenderer.setText(noteModel.getTitle());
			return defaultListCellRenderer;
		}
		return c;
	}

}
