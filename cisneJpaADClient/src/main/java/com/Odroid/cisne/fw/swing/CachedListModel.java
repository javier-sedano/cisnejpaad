package com.Odroid.cisne.fw.swing;

import javax.swing.AbstractListModel;

public abstract class CachedListModel<E> extends AbstractListModel<E> {
	private static final long serialVersionUID = 1L;

	private E[] cachedList = null;

	private int size = 0;

	protected CachedListModel() {
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public E getElementAt(int index) {
		if (cachedList[index] == null) {
			cachedList[index] = getModelElementAt(index);
		}
		return cachedList[index];
	}

	@SuppressWarnings("unchecked")
	public void setDirty() {
		if (size > 0) {
			fireIntervalRemoved(this, 0, size - 1);
		}
		size = getModelSize();
		cachedList = (E[]) new Object[size];
		if (size > 0) {
			fireIntervalAdded(this, 0, size - 1);
		}
	}

	public abstract int getModelSize();

	public abstract E getModelElementAt(int index);
}
